{ mkDerivation, base, containers, lib, tabular, tagsoup }:
mkDerivation {
  pname = "utschel";
  version = "0.1.0.0";
  src = ./.;
  isLibrary = false;
  isExecutable = true;
  executableHaskellDepends = [ base containers tabular tagsoup ];
  description = "Initial project template from stack";
  license = "unknown";
  mainProgram = "utschel-exe";
}
