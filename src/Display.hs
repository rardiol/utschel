{-# LANGUAGE NamedFieldPuns #-}

module Display where

import Data.List

import Text.Tabular
import Text.Tabular.AsciiArt

import Datas

type STable = Table (String) (String) String
type SSTable = SemiTable String String

tabelar_resumo :: (PAE, [CursoUnico]) -> String
-- tabelar_resumo ((periodos, dias), cursos) = show periodos ++ show dias ++ "\n" ++ render id id id $ tabler $ cursos
tabelar_resumo ((periodos, dias, horasaula), cursos) =
  render id id id extra_data_st ++
  render id id id main_table ++
  render id id id tabela_horarios
  where
    extra_data_st :: STable
    extra_data_st = Table (Header "") (Header "") [["Periodos:" ++ show periodos ++ " Dias:" ++ show dias ++ " Horas Aula:" ++ show horasaula]]
    main_table :: STable
    main_table = tabler cursos
    tabela_horarios :: STable
    tabela_horarios = tabela_horarios_basica $ tablerh cursos

tabler :: [CursoUnico] -> STable
tabler cursos = Table
  (Group SingleLine
     [
       Group NoLine $ map (\_ -> Header "") cursos
     ])
  (Group DoubleLine
     [ Group SingleLine [Header "Codigo", Header "Turma", Header "Nome", Header "Optativa"]
     ])
  (map single_line cursos)
  where
    single_line :: CursoUnico -> [String]
    single_line cuun@(CursoUnico curso oferecimento) = [codigo curso, turma oferecimento, nome curso, if optativa cuun then "X" else " "]

tablerh :: [CursoUnico] -> [[String]]
tablerh cursos = map (\x -> map temaula x) matrizhorarios
  where
    matrizhorarios :: [[String]]
    matrizhorarios = map f1 hor_lines
    f1 :: String -> [String]
    f1 line = map (\c -> c ++ line) hor_columns

    temaula :: String -> String
    temaula horario2 = temaula2 $ find (\(CursoUnico curso Oferecimento {encontros}) -> elem horario2 (map horario encontros) ) cursos 
    temaula2 :: Maybe CursoUnico -> String
    temaula2 Nothing = " "
    temaula2 (Just (CursoUnico (Curso {codigo}) _)) = codigo

tabela_horarios_basica :: [[String]] -> STable
tabela_horarios_basica = Table
  (Group SingleLine
     [
       Group NoLine $ map Header $ hor_lines
     ]
  )
  (Group DoubleLine
     [ Group SingleLine $ map Header $ hor_columns
     ])

hor_columns :: [String]
hor_columns = map (\x -> (show x) ) [2..6]
hor_lines :: [String]
hor_lines =
  (map (\x -> "M" ++ (show x) ) [1..6]) ++
  (map (\x -> "T" ++ (show x) ) [1..6]) ++
  (map (\x -> "N" ++ (show x) ) [1..5])
