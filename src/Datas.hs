
{-# LANGUAGE NamedFieldPuns #-}

module Datas where

import Data.Char
import Data.List

data CursoUnico = CursoUnico Curso Oferecimento deriving (Show , Eq , Ord)

data Curso = Curso {
     codigo :: String
   , nome :: String
   , aulasSemanais :: Integer
   , oferecimentos :: [Oferecimento]
  } deriving (Show, Ord, Eq)

data Oferecimento = Oferecimento {
    turma :: String
  , vagas :: Integer
  , vagas_calouros :: Integer
  , reserva :: Reserva
  , prioridade :: String -- Prioridade
  , encontros :: [Encontro]
  , professor :: Professor
  , optativas :: [String] -- Optativa
  } deriving (Show, Ord, Eq)

type Professor = String

data Reserva = Fechada | Aberta | SemReserva deriving (Show, Ord, Eq)

data Encontro = Encontro {
    sala :: String
  , horario :: Horario
  } deriving (Show, Ord, Eq)

type Horario = String

todosHorarios :: [CursoUnico] -> [Horario]
todosHorarios = mconcat . (map getHor)
  where
    getHor :: CursoUnico -> [Horario]
    getHor (CursoUnico _ (Oferecimento {encontros})) = map horario encontros


optativa :: CursoUnico -> Bool
optativa (CursoUnico _ (Oferecimento {optativas})) = or $  map (isInfixOf "Comput") optativas

type PAE = (Int, Int, Int)
