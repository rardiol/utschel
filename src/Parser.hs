module Parser where

import Debug.Trace
import Data.Maybe
import Data.List
import Data.Char

import Text.HTML.TagSoup

import Datas

myparse :: [Tag String] -> [Curso]
myparse s = map parseSingleCurso partitionedByCurso
  where
    partitionedByCurso :: [[Tag String]]
    partitionedByCurso = partitions isBegginingOfCurso s
    isBegginingOfCurso :: Tag String -> Bool
    isBegginingOfCurso td = td == TagOpen "td" [("class", "t"), ("colspan", "11")]

-- Recebe a lista de tags comecando com <td class="curso"
-- trace ("FOO: " ++ (show foo))
parseSingleCurso :: [Tag String] -> Curso
parseSingleCurso foo@(td_curso:_:_:_:nome_e_codigo_tag:_:aulas_semanais_tag:resto) =  Curso {
  codigo = codigo,
  nome = nome,
  aulasSemanais = aulasSemanais,
  oferecimentos = oferecimentos
  }
  where
    oferecimentos_tags = init $ drop 37 resto
    oferecimento_split :: [[Tag String]]
    oferecimento_split = filter isRealOferecimento $ partitions isBegginingOfOferecimento oferecimentos_tags
    isBegginingOfOferecimento :: Tag String -> Bool
    isBegginingOfOferecimento td = td == TagOpen "tr" []
    isRealOferecimento :: [Tag String] -> Bool
    isRealOferecimento tags = (length tags) > 1
    oferecimentos :: [Oferecimento]
    oferecimentos = map parseSingleOferecimento oferecimento_split

    nome_e_codigo =trace (show nome_e_codigo_tag) $  fromTagText nome_e_codigo_tag
    codigo = take 5 nome_e_codigo
    nome = drop 8 nome_e_codigo
--  aulas_semanais_text = trace ("AST:" ++ (show aulas_semanais_tag)) $ get_aulas_semanais_text aulas_semanais_tag
    aulas_semanais_text = get_aulas_semanais_text aulas_semanais_tag
    get_aulas_semanais_text :: Tag String -> String
    get_aulas_semanais_text (TagText str) = str
    get_aulas_semanais_text _ = "0"
    aulasSemanais :: Integer
    aulasSemanais = read $ takeWhile isDigit $ dropWhile (not . isDigit) aulas_semanais_text

parseSingleOferecimento :: [Tag String] -> Oferecimento
parseSingleOferecimento aa@(
  _
  :_
  :_
  :(TagText turma_text)
  :_
  :(TagText "\n  ")
  :(TagOpen _ _)
  :(TagClose _)
  :(TagText _)
  :(TagOpen _ _)
  :(TagText _)
  :(TagClose _)
  :(TagText _)
  :(TagOpen _ _)
  :(TagText presencial_text)
  :(TagClose _)
  :(TagText _)
  :(TagOpen _ _)
  :(TagText vagas_text):_:_:_:TagText vagas_calouros_text:_:_:_:TagText reserva_text:_:resto1) = parseSingleOferecimento2 turma_text presencial_text vagas_text vagas_calouros_text reserva_text resto1

parseSingleOferecimento aa@(
  _
  :_
  :_
  :(TagText turma_text)
  :_
  :_
  :_
  :_
  :_
  :_
  :_
  :(TagText presencial_text)
  :_
  :_
  :_
  :(TagText vagas_text)
  :_
  :_
  :_
  :(TagText vagas_calouros_text)
  :_
  :_
  :_
  :(TagText reserva_text)
  :_
  :resto1) = parseSingleOferecimento2 turma_text presencial_text vagas_text vagas_calouros_text reserva_text resto1

parseSingleOferecimento aa = error (trace (show aa) (show aa))

parseSingleOferecimento2 :: String -> String -> String -> String -> String -> [Tag String] -> Oferecimento
parseSingleOferecimento2 turma_text presencial_text vagas_text vagas_calouros_text reserva_text resto1 = Oferecimento {
  turma = turma_text,
  vagas = read vagas_text,
  vagas_calouros = read vagas_calouros_text,
--vagas = trace ("vagas:" ++ (show vagas_text)) read vagas_text,
--vagas_calouros = trace ("vagasc:" ++ (show vagas_calouros_text)) read vagas_calouros_text,
  reserva = parseReserva reserva_text,
  prioridade = unlines $ catMaybes $ map maybeTagText prioridade_tags,
  encontros = parseEncontros $ mconcat $ catMaybes $ map maybeTagText encontros_tags,
  professor = professor_text,
  optativas = mapMaybe optativaFilter $ map trim $ catMaybes $ map maybeTagText optativas_tags
  }
  where
    (prioridade_tags, resto2) = break (\x -> x == TagOpen "td" [("class", "dn")]) resto1
    (encontros_tags, resto3) = break (\x -> x == TagOpen "td" [("class", "ml")]) resto2
    (_:TagText professor_text:_:_:_:_:_:_:optativas_tags) =  resto3
    optativaFilter "" = Nothing
    optativaFilter "Não" = Nothing
    optativaFilter x = Just x


parseReserva :: String -> Reserva
parseReserva "Fechada" = Fechada
parseReserva "Aberta" = Aberta
parseReserva "Sem Reserva" = SemReserva

parseEncontros :: String -> [Encontro]
parseEncontros = (nubBy (\a b -> horario a == horario b)) . ((map parseEncontro) . (filter (\x -> x /= "-")) . words)

parseEncontro :: String -> Encontro
parseEncontro str = Encontro {
    horario = take 3 str
  , sala = drop 3 str
  }


trim = dropWhileEnd isSpace . dropWhile isSpace
