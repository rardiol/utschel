{-# LANGUAGE NamedFieldPuns #-}
{-# LANGUAGE ScopedTypeVariables #-}

-- Nota : Isso nao entende equivalencias

import Text.HTML.TagSoup

import qualified Data.Set as Set
import Data.List
import Data.Ord

import Debug.Trace

import Datas
import Parser (myparse)
import Display

main :: IO ()
main = do
  datafile <- readFile "data.html"
  let parsedTags = parseTags datafile
  putStrLn "Parsing Warnings:"
  print $ filter isTagWarning parsedTags
  let parsing = myparse parsedTags
  putStrLn "Parsing results length (parsing.txt):"
  print $ length parsing
  writeFile "parsing.txt" (show parsing)

  selectionFile <- readFile "selection.txt"
  let selecao :: ([String],[String],[String],[[String]]) = read selectionFile
  let (principais,secundarios,adicionais,equivalentes) = selecao

  aPAEFile <- readFile "pae.txt"
  let (maxDias, minHorasAula, maxHorasAula) :: (Int, Int, Int) = read aPAEFile

  foo (parsing, (principais,secundarios,adicionais), (maxDias, minHorasAula, maxHorasAula), equivalentes)

foo :: ([Curso], ([String], [String], [String]), (Int, Int, Int), [[String]]) -> IO ()
foo (parsing, (principais,secundarios,adicionais), (maxDias, minHorasAula, maxHorasAula), equivalentes) = do
  let dados_principais = filter (\x -> (codigo x) `elem` principais) parsing
  let dados_adicionais = filter (\x -> (codigo x) `elem` adicionais) parsing

  putStrLn "Principais (principais.txt):"
  --print dados_principais
  writeFile "principais.txt" (show principais)

  putStrLn "Possibilidade principais extra"
-- TODO: use summaryfilter?
  let possibilidades_principais =(possibilidadesCombinatorias dados_principais)
  let possibilidades_principais_extra = map dadosExtras possibilidades_principais
  --print $  possibilidades_principais_extra
  writeFile "principais_extra.txt" (show possibilidades_principais_extra)

  putStrLn "Numero de possibilidades principais:"
  print $ length $ possibilidades_principais

  putStrLn "Por Periodos:"
  print $ map fst possibilidades_principais_extra

  putStrLn "possibilidades_principais_extra:"
  print $ map resumir possibilidades_principais_extra
  -- mapM_ putStrLn $ map tabelar_resumo $ sortppe possibilidades_principais_extra
  writeFile "principais_extra.txt" $ concat $ map tabelar_resumo $ sortppe $ filter summaryFilter possibilidades_principais_extra

  putStrLn "Adicionais, numero:"
  let possibilidades_adicionais = possibilidadesCombinatoriasAdicionais dados_principais dados_adicionais
  let possibilidades_adicionais_extra_base = map (\x -> map dadosExtras x) possibilidades_adicionais
  print $ length $ possibilidades_adicionais_extra_base
  putStrLn "Adicionais filtradas (adicionais.txt), numero:"
  let possibilidades_adicionais_extra = summaryFilterPAE possibilidades_adicionais_extra_base
  print $ length $ possibilidades_adicionais_extra
  --print $ map (\x -> map resumir x) possibilidades_adicionais_extra
  writeFile "adicionais.txt" $ show $ map (\x -> map resumir x) possibilidades_adicionais_extra

  putStrLn "Adicionais tabelados (adicionais_tabelados.txt)"
  -- mapM_ putStrLn $ map unlines $ map (\x -> map tabelar_resumo x) $ sortpae possibilidades_adicionais_extra
  writeFile "adicionais_tabelados.txt" $ concat $ map unlines $ map (\x -> map tabelar_resumo x) $ sortpae possibilidades_adicionais_extra

  where

    sortppe = sortBy sortppe2
    sortppe2 ((_,_,a),_) ((_,_,b),_) = compare a b
    sortpae = sortBy sortpae2
    sortpae2  (((_,_,a),_):_) (((_,_,b),_):_) = compare a b
    sortpae2  [] _ = LT
    sortpae2  _ [] = GT

    summaryFilter :: (PAE, [CursoUnico]) -> Bool
    summaryFilter ((numtotalPeriodos, numtotalDias, numtotalHorasAula), _) = and [numtotalDias < maxDias, numtotalHorasAula > minHorasAula, numtotalHorasAula < maxHorasAula]
    summaryFilterPAE :: [[(PAE, [CursoUnico])]] -> [[(PAE, [CursoUnico])]]
    summaryFilterPAE dat = filter (not . null) $ map summaryFilterPAE2 dat
    summaryFilterPAE2 :: [(PAE, [CursoUnico])] -> [(PAE, [CursoUnico])]
    summaryFilterPAE2 = (filter summaryFilter) . (filter equivalenteFilter)

--horariosNaoBatem l = (length l) == ((Set.size $ Set.fromList l))
    equivalenteFilter :: (PAE, [CursoUnico]) -> Bool
    equivalenteFilter (_, cursos) = all (<= 1) $ trace ("DW:" ++ (show $ map (length . (Set.intersection (Set.fromList $ map getCodigo cursos))) equivalentesSet)) $ map (length . (Set.intersection (Set.fromList $ map getCodigo cursos))) equivalentesSet
    getCodigo (CursoUnico curso _) = codigo curso
    equivalentesSet = map Set.fromList equivalentes
      

dadosExtras :: [CursoUnico] -> (PAE, [CursoUnico])
dadosExtras x = ((totalPeriodos x, totalDias x, totalHorasAula x), x)

resumir :: (PAE, [CursoUnico]) -> (PAE, [(String, String)])
resumir (a, cursos) = (a, map resumir2 cursos)
  where
    resumir2 :: CursoUnico -> (String, String)
    resumir2 (CursoUnico (Curso {codigo}) (Oferecimento {turma})) = (codigo, turma)

-- data Horario = Horario {
--     dia :: Dia
--   , turno :: Turno
--   , aula :: Aula
--   } deriving (Show, Eq)

-- data Dia = D2 | D3 | D4 | D5 | D6 deriving (Show, Eq)
-- data Turno = M | T | N deriving (Show, Eq)
-- data Aula = A1 | A2 | A3 | A4 | A5 | A6 deriving (Show, Eq)


horariosNaoBatem :: [Horario] -> Bool
horariosNaoBatem l = (length l) == ((Set.size $ Set.fromList l))

-- 3M1, 3M2, 4M1 -> 2, 2M1, 2T1, 2N1 -> 3
totalPeriodos :: [CursoUnico] -> Int
totalPeriodos = (Set.size) . (Set.fromList) . (map (take 2)) . todosHorarios

totalDias :: [CursoUnico] -> Int
totalDias = (Set.size) . (Set.fromList) . (map (take 1)) . todosHorarios

totalHorasAula :: [CursoUnico] -> Int
totalHorasAula = (Set.size) . (Set.fromList) . todosHorarios

possivel :: [CursoUnico] -> Bool
possivel a = horariosNaoBatem $ todosHorarios a

possibilidadesCombinatoriasAdicionais :: [Curso] -> [Curso] -> [[[CursoUnico]]] -- Uma Lista de possibilidades de cursos, cada qual contendo uma lista de possibilidades de oferecimentos, cada qual contendo uma lista de cursosunicos
possibilidadesCombinatoriasAdicionais cursos_obrigatorios cursos_adicionais = map possibilidadesCombinatorias cursos
  where
    cursos :: [[Curso]]
    cursos = map (cursos_obrigatorios ++) (subsequences cursos_adicionais)

possibilidadesCombinatorias :: [Curso] -> [[CursoUnico]]
possibilidadesCombinatorias cursos = filter possivel cursosUnicosPorPossibilidade
  where
    cursosUnicosPorCurso :: [[CursoUnico]]
    cursosUnicosPorCurso = map genCursoUnicos cursos

    cursosUnicosPorPossibilidade :: [[CursoUnico]]
    cursosUnicosPorPossibilidade = sequence cursosUnicosPorCurso

-- This filters EADs e sem vagas
genCursoUnicos :: Curso -> [CursoUnico]
genCursoUnicos curso@Curso {oferecimentos} = 
--  filter (horario_filter okall) $
--  filter (horario_filter horarioNotMorning) $
--  filter (horario_filter horarioDiurno) $
--  filter com_vaga $
--  filter e01_filter $
--  filter ead_filter $
--  filter (\x -> (diurno x || optativa x)) $
  map (CursoUnico curso) oferecimentos
  where
    ead_filter :: CursoUnico -> Bool
    ead_filter (CursoUnico _ (Oferecimento {turma})) = turma /= "EAD"
    e01_filter :: CursoUnico -> Bool
    e01_filter (CursoUnico _ (Oferecimento {turma})) = turma /= "E01"
    com_vaga :: CursoUnico -> Bool
    com_vaga (CursoUnico _ (Oferecimento {vagas})) = vagas > 0
    horario_filter :: (Horario -> Bool) -> (CursoUnico -> Bool)
    horario_filter filtro (CursoUnico _ (Oferecimento {encontros})) = all filtro $ map horario encontros
    horarioDiurno (_:'N':"1") = True
    horarioDiurno (_:'N':"2") = True
    horarioDiurno (_:'N':_) = False
    horarioDiurno (_:'M':_) = True
    horarioDiurno (_:'T':_) = True
    horarioNotMorning (_:'M':"6") = True
    horarioNotMorning (_:'M':_) = False
    horarioNotMorning (_:_:_) = True
    horarioNotTarde (_:'T':_) = False
    horarioNotTarde (_:_:_) = True
